# Tracking Tool #

Most commonly used tracking tools:

- Google Analytics Tracking
- Convert Tracking
- Optimizely Tracking
- Adobe Target Tracking

### How do I get set up? ###

If you're using [Base Template - Vanilla](https://www.notion.so/teamcroco/Base-Template-Vanilla-e2818ba08236453b816a0bdbabfad8ac), look for ES5 snippets.

[Download Here](https://bitbucket.org/lecamen/tracking-tool/downloads/)

---

If you're using [Ticket Development Tool](https://bitbucket.org/lecamen/ticket-development-tool/src/master/) then import tool from packages.

---

If you're using [Ticket Development Tool - Vanilla](https://bitbucket.org/lecamen/ticket-development-tool-vanilla/src/master/) then import tool from packages.

```jsx

// Adobe Target
import adobeTargetSend from './[DIR]/adobe-target';

// Convert
import convertSend from './[DIR]/convert';

// Google Analytics for Conversion
import gaSend from './[DIR]/google-analytics/conversion';

// Google Analytics for Online Dialogue
import gaSend from './[DIR]/google-analytics/online-dialogue';

// Google Analytics Standard
import gaSend from './[DIR]/google-analytics/standard';

// Google Analytics Custom
import gaSend from './[DIR]/google-analytics/custom';

// Optimizey
import optimizelySend from './[DIR]/optimizely';

```