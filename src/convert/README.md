# Convert Tracking #

This method send data to convert.

---

**How do I get set up?**

```jsx

// Import from specific directory.

import convertSend from './[DIR]/convert';

// if not using node module 
// copy snippet from /[DIR]/convert/es5/convert.es5.min.js

```

## Usage

```jsx
// Goal ID

convertSend(10024835);

// Example ( Convert Global not Webpack)
// If not webpack copy snippet from webpack to Convert

const $ = $convert.$

$(window).load(function(){

	$('.cta').on('click', function(){

		window._conv_q = window._conv_q || [];
    	_conv_q.push(["triggerConversion", 10024835]);

	});

});
```