# GA Tracking #

There are custom tracking format:

- [Standard](https://bitbucket.org/lecamen/trackings/src/master/src/google-analytics/standard/)
- [For Online Dialogue ( Only )](https://bitbucket.org/lecamen/trackings/src/master/src/google-analytics/online-dialogue/)

**How do I get set up?**

```jsx

// Import from specific directory.

import gaSend from './[DIR]/google-analytics/standard';

// if not using node module 
// copy snippet from /[DIR]/google-analytics/standard/es5/optimizely.es5.min.js

// or

import gaSend from './[DIR]/google-analytics/online-dialogue';

// if not using node module 
// copy snippet from /[DIR]/google-analytics/online-dialogue/es5/optimizely.es5.min.js

```

## Usage