
let trackerIndex = 0,
    GAindex = 0

const gaTracker = ( config, callback ) => {

    if ( !config || ( config && !'gaid' in config ) ) {
        
        console.log('Please check tool configuration')
        
        return

    }

    const googleAnalyticsObject = window[window['GoogleAnalyticsObject']] || window.ga || ga || undefined

    if ( googleAnalyticsObject && googleAnalyticsObject.create ) {
 
        let allTrackers = googleAnalyticsObject.getAll(),
            i

        for (i = 0; i < allTrackers.length; i++) {
           
            if ( allTrackers[i].get('trackingId') === config.gaid ) {
                
                callback( allTrackers[i] )

                break

            } else if  ( i === allTrackers.length - 1 ) {
                
                if (trackerIndex !== 200) {

                    console.warn('Tracker not found, Trying again')

                    setTimeout( () => {

                        gaTracker( config, callback )

                    }, 50)

                    trackerIndex++

                } else {

                    console.error("TRACKER: \"".concat(config.gaid, "\" NOT FOUND"))

                }
            }
        }

    } else if (GAindex != 200) {

        setTimeout( () => {

            gaTracker( config, callback )

        }, 350)

        GAindex++

    } else {

        console.error('Google Analytics not found!')

    }
}

export default gaTracker
