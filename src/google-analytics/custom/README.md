# Custom GA Tracking #

This method send data to Google Analytics ( Customized ).

---

**How do I get set up?**

```jsx

// Import from specific directory.

import gaSend from './[DIR]/google-analytics/custom';

// if not using node module 
// copy snippet from /[DIR]/google-analytic/online-dialogue/es5/custom.es5.min.js

```

**Configurations**

```jsx
/*
* GA Config
*/
var toolConfig = {
    gaid: 'UA-49794614-1', // GA UID
    testId: 'OD235', // Client Test ID
    category: 'Video Playback', // Category
    variant: 'B: Variant' // Variation
};
```

**Parameters**

```jsx
/*
* @config
* Tool configuration
*/

/*
* @toolConfig : Custom configuration
* @eventAction : e.g "pageview" or "Custom action event"
* @eventLabel : e.g "Custom label event"
* @Object : { nonInteraction: false } - True or False
*/

gaSend(toolConfig, eventAction, { nonInteraction: false })

gaSend(toolConfig, eventAction, eventLabel, { nonInteraction: false })

```

**Usage**

```jsx
/*
* GA Config
* See more config above
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX'
	...,
	...,
	...
}

/* GA Send */
function gaSend(o){ ... }
/* END - GA Send */

/*
* Sample click event
*/
document.body.addEventListener('click', function(){

	gaSend(toolConfig, 'eventAction', { nonInteraction: false } )

	// Extra
	gaSend(toolConfig, 'eventAction', 'eventLabel', { nonInteraction: false } )

})

/*
* Sample Page view
*/
gaSend(toolConfig, 'pageview')

```