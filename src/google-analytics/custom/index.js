/*
* GA Tracking for Online Dialogue
*/

import gaTracker from "../tracker"

const gaSend = function( config ) {

    const args = [
        ...arguments
    ]

    gaTracker( config, tracker => {

        const pageView = 'pageview'

        if ( tracker ) {

            args.shift()

            let hitType = args.length && args[0] !== undefined && args[0] === pageView ? args[0] : 'event',
                category = `${ config.testId ? config.testId + ' : ' + config.category  : config.category  }`,
                action = `${ config.variant } | ${ args[0] }`,
                label = args[1] !== undefined && typeof args[1] !== 'object' ? args[1] : '',
                nonInteraction = args.length && args[ args.length - 1 ] !== undefined && typeof args[ args.length - 1 ] === 'object' ? args[ args.length - 1 ] : undefined

            if ( hitType === pageView ) {

                tracker.send.apply( tracker, [
                    hitType
                ])

            } else {
                
                tracker.send.apply( tracker, [
                    hitType,
                    category,
                    action,
                    label, 
                    nonInteraction
                ])

            }

        }

    })

}

gaSend()

export default gaSend