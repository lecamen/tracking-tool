function gaSend(config) {

    var args = [].concat(Array.prototype.slice.call(arguments));

    var GAindex = 0,
        trackerIndex = 0;

    var gaTracker = function() {

        const googleAnalyticsObject = window[window['GoogleAnalyticsObject']] || window.ga || ga || undefined

        if ( googleAnalyticsObject && googleAnalyticsObject.create ) {

            var trackers = googleAnalyticsObject.getAll(),
                i = void 0;

            for (i = 0; i < trackers.length; i++) {

                if (trackers[i].get('trackingId') === config.gaid) {

                    args.shift();

                    trackers[i].send.apply(trackers[i], args);

                    break;

                } else if (i === trackers.length - 1) {

                    if (trackerIndex !== 200) {

                        console.warn('Tracker not found, Trying again');

                        setTimeout(function () {

                            gaTracker();

                        }, 50);

                        trackerIndex++;

                    } else {

                        console.error("TRACKER: \"".concat(config.gaid, "\" NOT FOUND"));

                    }
                }
            }
        } else if (GAindex != 200) {

            setTimeout(function () {

                gaTracker();

            }, 350);

            GAindex++;

        } else {

            console.error('Google Analytics not found!');

        }
    };

    gaTracker();

}