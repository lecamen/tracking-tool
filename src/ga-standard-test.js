import gaSend from "./google-analytics/standard"

const toolConfig = {
    gaid: 'UA-107242044-1'
}

gaSend(toolConfig, 'pageview', { nonInteraction: true })

document.body.addEventListener('click', () => {
 
    gaSend(toolConfig, 'event', 'Category', 'Action', { nonInteraction: false })

})