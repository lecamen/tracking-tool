# Adobe Target Tracking #

This method send data to Adobe Target.

---

**How do I get set up?**

```jsx

// Import from specific directory.

import adobeTargetSend from './[DIR]/adobe-target';

// if not using node module 
// copy snippet from /[DIR]/adobe-target/es5/adobe-target.es5.min.js

```

**Usage**

```jsx
adobeTargetSend ('User click on CTA');

// Or With Adobe extra options

const value = 'User click on CTA'

adobeTargetSend(
	value,
	{
		linkTrackVars: 'eVar89',
		eVar89: value
	}
)
```